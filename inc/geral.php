<?
$nomeSite			= 'Galpão em Lona';
$slogan				= 'Simplesmente o melhor do ramo!';

$dir = $_SERVER['SCRIPT_NAME'];
$dir = pathinfo($dir);
$host = $_SERVER['HTTP_HOST'];
$http = $_SERVER['REQUEST_SCHEME'];
if ($dir["dirname"] == "/") { $url = $http."://".$host."/"; }
else { $url = $http."://".$host.$dir["dirname"]."/";  }


$emailContato		= 'everton.lima@solucoesindustriais.com.br';
$rua				= 'Rua Pequetita, 179';
$bairro				= 'Vila Olimpia';
$cidade				= 'São Paulo';
$UF					= 'SP';
$cep				= 'CEP: 04552-060';
$latitude			= '-22.546052';
$longitude			= '-48.635514';
$idAnalytics		= 'UA-119965399-16';
$senhaEmailEnvia	= '102030'; // colocar senha do e-mail mkt@dominiodocliente.com.br
$explode			= explode("/", $_SERVER['PHP_SELF']);
$urlPagina 			= end($explode);
$urlPagina	 		= str_replace('.php','',$urlPagina);
$urlPagina 			== "index"? $urlPagina= "" : "";
$urlAnalytics = str_replace("http://www.", '', $url);
$urlAnalytics = str_replace("/", '', $urlAnalytics);
//reCaptcha do Google
$siteKey = '6Lfc7g8UAAAAAHlnefz4zF82BexhvMJxhzifPirv';
$secretKey = '6Lfc7g8UAAAAAKi8al32HjrmsdwoFoG7eujNOwBI';


//Breadcrumbs
include('inc/categoriasGeral.php');
$caminho  = '<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >';
$caminho .= '<a rel="home" itemprop="url" href="'.$url.'" title="Home">';
$caminho .= '<span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »';
$caminho .= '<strong><span class="page" itemprop="title" > '.$h1.'</span></strong></div>';

$caminho2	 = '<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >';
$caminho2	.= '<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title">';
$caminho2	.= '<i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »';
$caminho2	.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminho2	.= '<a href="'.$url.'produtos" title="Produtos" class="category" itemprop="url">';
$caminho2	.= '<span itemprop="title"> Produtos </span></a> »';
$caminho2	.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminho2	.= '<strong><span class="page" itemprop="title" > '.$h1.'</span></strong></div></div></div>';

$caminhogalpao_em_lona = '<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >';
$caminhogalpao_em_lona.= '<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title">';
$caminhogalpao_em_lona.= '<i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »';
$caminhogalpao_em_lona.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhogalpao_em_lona.= '<a href="'.$url.'produtos" title="Produtos" class="category" itemprop="url">';
$caminhogalpao_em_lona.= '<span itemprop="title"> Produtos </span></a> »';
$caminhogalpao_em_lona.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhogalpao_em_lona.= '<a href="'.$url.'galpao-em-lona-categoria" title="Categoria - Galpão em Lona" class="category" itemprop="url">';
$caminhogalpao_em_lona.= '<span itemprop="title"> Categoria - Galpão em Lona </span></a> »';
$caminhogalpao_em_lona.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhogalpao_em_lona.= '<strong><span class="page" itemprop="title" > '.$h1.'</span></strong></div></div></div></div>';

$caminhogalpao_pre_moldado  = '<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >';
$caminhogalpao_pre_moldado .= '<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title">';
$caminhogalpao_pre_moldado .= '<i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »';
$caminhogalpao_pre_moldado .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhogalpao_pre_moldado .= '<a href="'.$url.'produtos" title="Produtos" class="category" itemprop="url">';
$caminhogalpao_pre_moldado .= '<span itemprop="title"> Produtos </span></a> »';
$caminhogalpao_pre_moldado .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhogalpao_pre_moldado .= '<a href="'.$url.'galpao-pre-moldado-categoria" title="Categoria - Galpão Pré Moldado " class="category" itemprop="url">';
$caminhogalpao_pre_moldado .= '<span itemprop="title"> Categoria - Galpão Pré Moldado </span></a> »';
$caminhogalpao_pre_moldado .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhogalpao_pre_moldado .= '<strong><span class="page" itemprop="title" > '.$h1.'</span></strong></div></div></div></div>';
    


?>